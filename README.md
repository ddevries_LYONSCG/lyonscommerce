# LYONS Commerce

## LYONSCG U Project 2
Davis DeVries

___

### A mock eCommerce store using the Angular 4 framework and an Express.js back-end server (that makes API calls to OCAPI).

____

## Setup

```
git clone https://ddevries_LYONSCG@bitbucket.org/ddevries_LYONSCG/lyonscommerce.git
cd lyonscommerce
npm install
npm start
cd angular-src
npm install
npm start
```
The express server should run on localhost:3000.

The angular cli should run on localhost:4200 (see more within the README.md within /angular-src).
