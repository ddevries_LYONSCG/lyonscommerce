"use strict";

const PORT = 3000;

// Require //
const express = require('express');
const path = require('path');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const https = require('https');
const fetch = require('node-fetch')

// Initalizations //
const app = express();

// Middlware //
app.use(cors());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client')));

app.listen(PORT, () => {
    console.log(`Server started on port: ${PORT}`);
});

// Allow the GET request with Fetch
// https://github.com/bitinn/node-fetch/issues/19
const agent = new https.Agent({
    rejectUnauthorized: false,
});

function getSrc(html) {
    if (!html) return undefined;
    // https://stackoverflow.com/questions/450108/regular-expression-to-extract-src-attribute-from-img-tag
    let re = 'src\s*=\s*"(.+?)"';
    let src = (html.match(re)[0]);
    return src.slice(5, (src.length - 1));
}


//GET categories specified at root and level 1
app.get('/categories', (req, res, next) => {
    let url = `https://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v18_1/categories/root?levels=1&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`;
    fetch(url, {
            method: "GET",
            agent
        })
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            let categories = data.categories;
            let resObj = [];
            for (let i = 0; i < categories.length; i++) {
                resObj.push({
                    "id": categories[i].id,
                    "name": categories[i].name,
                    "description": categories[i].page_description || null,
                    "imageSrc": getSrc(categories[i].c_headerMenuBanner) || 'https://aea.digitellinc.com/assets/img/image_placeholder_banner.jpg'
                });

            }
            res.send(resObj);
            res.end();
        })
        .catch((err) => {
            console.log("error caught in /categories");
            console.log(err);
            next();
        });
});

// GET sub-categories by an individual ID
app.get('/categories/:id', (req, res, next) => {
    let url = `https://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v18_1/categories/${req.params.id}?&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`
    fetch(url, {
            method: "GET",
            agent
        })
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            let categories = data.categories;
            let resObj = [];
            if (categories) {
                for (let i = 0; i < categories.length; i++) {
                    resObj.push({
                        "id": categories[i].id,
                        "name": categories[i].name,
                        "title": categories[i].page_title,
                        "description": categories[i].page_description || null,
                        "img": categories[i].image
                    });
                }
            }
            res.send(resObj);
            res.end();
        })
        .catch((err) => {
            console.log("error caught in /categories/:id");
            console.log(err);
            next();
        });
});

// GET products by a query
app.get('/products/:query', (req, res, next) => {
    console.log(req.query.q)
    console.log(req.params.query);
    let url = '';
    if (!req.query.q) {
        url = `https://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v18_1/product_search?q=${req.params.query}&expand=prices,images&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`;
    }
    else {
        url = `https://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v18_1/product_search?q=${req.params.query}&expand=prices,images&start=${req.query.q}&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`
    }
    fetch(url, {
            method: "GET",
            agent
        })
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            let products = data.hits;
            let resObj = [];
            for (let i = 0; i < products.length; i++) {
                resObj.push({
                    "id": products[i].product_id,
                    "name": products[i].product_name,
                    "price": products[i].price,
                    "img": products[i].image.link
                });
            }
            res.send(resObj);
            res.end();
        })
        .catch((err) => {
            console.log("error caught in /products/:query");
            console.log(err);
            next();
        });
});

// GET an individual product by ID
app.get('/products/product/:id', (req, res, next) => {
    let url = `https://lyons5.evaluation.dw.demandware.net/s/SiteGenesis//dw/shop/v18_1/products/${req.params.id}?&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`;
    console.log(url);
    fetch(url, {
            method: "GET",
            agent
        })
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            let product = data;
            res.send({"description": product.short_description});
            res.end();
        })
        .catch((err) => {
            console.log("error caught in /products/:query");
            console.log(err);
            next();
        });
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(404);
    res.write('Something went wrong!');
    res.end();
});