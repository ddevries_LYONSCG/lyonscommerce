// main modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

// animation modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// additional 'style' helper modules
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { MaterializeModule } from 'angular2-materialize';

// infinite scroll
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// components
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ProductSearchComponent } from './components/product-search/product-search.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductComponent } from './components/product/product.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CategoryComponent } from './components/category/category.component';

const appRoutes: Routes = [
  { path: '', component: AppComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProductSearchComponent,
    ProductsComponent,
    ProductComponent,
    CategoriesComponent,
    CategoryComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
    NgbModule,
    MaterializeModule,
    InfiniteScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
