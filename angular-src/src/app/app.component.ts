import {
  Component,
  Input,
  Output,
  OnInit,
  EventEmitter
} from '@angular/core';
import {
  Http,
  Headers
} from '@angular/http';
import 'rxjs/add/operator/map';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';
import {
  error
} from 'util';

@Component({
  selector: 'lyonscg-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  @Input() categorySearch = new EventEmitter < string > ();

  title = 'Lyons Commerce';
  query: string;
  invalidSearch: boolean;
  products: any[];

  constructor() {
    this.query = '';
    this.products = [];
    this.invalidSearch = false;
  }

  receiveCategoryQuery($event) {
    this.query = $event;
    this.invalidSearch = false;
  }

  productsPaginationRequest($event) {
    console.log($event);
    const index = $event;
    fetch(`http://localhost:3000/products/${this.query}?q=${index}`, {
        method: 'GET',
      })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        for (let i = 0; i < data.length; i++) {
          this.products.push(data[i]);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  receiveSearchQuery($event) {
    this.query = $event;
    this.products = [];
    if (!this.query) {
      this.invalidSearch = false;
      return;
    }
    fetch(`http://localhost:3000/products/${this.query}`, {
        method: 'GET',
      })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.invalidSearch = false;
        for (let i = 0; i < data.length; i++) {
          this.products.push(data[i]);
        }
      })
      .catch((err) => {
        console.log(err);
        this.invalidSearch = true;
        this.products = [];
      });
  }
}
