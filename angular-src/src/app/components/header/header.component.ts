import { Component, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';

@Component({
  selector: 'lyonscg-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  animations: [
    trigger('scaling', [
      state('scale-hide', style({
        opacity: 0,
        transform: 'scale(0)'
      })),
      state('scale-show', style({
        opacity: 1,
        transform: 'scale(1)'
      })),
      transition('scale-hide => scale-show', [
        animate('500ms ease')
      ])
    ]),
  ]
})
export class HeaderComponent implements OnInit {
  state: String;
  constructor() {
    this.state = 'scale-hide';
   }

   ngOnInit() {
    setTimeout(()=>{ 
      this.toggleScale();
    }, 500);
  }

  toggleScale() {
    this.state = this.state === 'scale-hide' ? 'scale-show' : 'scale-hide';
  }

}
