import { Component, OnInit, Input } from '@angular/core';

import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'lyonscg-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  animations: [
    trigger('scale-up', [
      state('scale-up-start', style({
        transform: 'translateY(250px)',
        opacity: 0
      })),
      state('scale-up-end', style({
        transform: 'translateY(0px)',
        opacity: 1,
      })),
      transition('scale-up-start => scale-up-end', [
        animate('1.8s ease')
      ])
    ]),
  ]
})
export class ProductComponent implements OnInit {
  @Input() product: any;
  description: string;
  state: string;

  constructor() {
    this.state = 'scale-up-start';
   }

   ngOnInit() {
    fetch(`http://localhost:3000/products/product/${this.product.id}`, {
      method: 'GET'
    })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      this.description = data.description;
    })
    .catch((err) => {
      console.log(err);
    });

    setTimeout(() => {
      this.toggleScale();
    }, 500);
  }

  toggleScale() {
    this.state = this.state === 'scale-up-start' ? 'scale-up-end' : 'scale-up-start';
  }

}
