import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'lyonscg-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  animations: [
    trigger('scale-up', [
      state('scale-up-start', style({
        transform: 'translateY(200px)',
        opacity: 0
      })),
      state('scale-up-end', style({
        transform: 'translateY(0px)',
        opacity: 1,
      })),
      transition('scale-up-start => scale-up-end', [
        animate('1.3s ease')
      ])
    ]),
  ]
})
export class ProductsComponent implements OnInit {
  @Input() products: any[];

  state: string;
  startingIndex: number;

  @Output() getMoreProducts = new EventEmitter<number>();

  constructor() {
    this.state = 'scale-up-start';
    this.startingIndex = 26;
   }

   ngOnInit() {
    setTimeout(() => {
      this.toggleScale();
    }, 500);
  }

  toggleScale() {
    this.state = this.state === 'scale-up-start' ? 'scale-up-end' : 'scale-up-start';
  }

  onScroll() {
    console.log('emitting scroll event');
    console.log(this.startingIndex);
    this.getMoreProducts.emit(this.startingIndex);
    this.startingIndex += 25;
  }
}
