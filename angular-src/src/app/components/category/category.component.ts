import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import {DomSanitizer} from '@angular/platform-browser';
import { CategoriesComponent } from '../categories/categories.component';

@Component({
  selector: 'lyonscg-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  animations: [
    trigger('scale-up', [
      state('scale-up-start', style({
        transform: 'translateY(200px)',
        opacity: 0
      })),
      state('scale-up-end', style({
        transform: 'translateY(0px)',
        opacity: 1,
      })),
      transition('scale-up-start => scale-up-end', [
        animate('1.25s ease')
      ]),
      transition('scale-up-end => scale-up-start', [
        animate('1.25s ease')
      ])
    ])
  ]
})
export class CategoryComponent implements OnInit, OnDestroy {
  @Input() category: any;
  state: string;
  subCategories: any[];

  @Output() categoryClick = new EventEmitter<string>();

  constructor() {
    this.state = 'scale-up-start';
    this.subCategories = [];
   }

  ngOnInit() {

    setTimeout(() => {
      this.toggleScale();
    }, 500);

    fetch(`http://localhost:3000/categories/${this.category.id}`, {
      method: 'GET'
    })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      for (let i = 0; i < data.length; i++) {
        this.subCategories.push(data[i]);
      }
    })
    .catch((err) => {
      console.log(err);
    });
  }

  ngOnDestroy() {
    console.log('ngOnDestroy');
    this.toggleScale();
  }

  toggleScale() {
    this.state = this.state === 'scale-up-start' ? 'scale-up-end' : 'scale-up-start';
  }

  onClickCategory(query) {
    this.categoryClick.emit(query);
  }
}
