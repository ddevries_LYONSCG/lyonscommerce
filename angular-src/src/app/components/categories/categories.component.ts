import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'lyonscg-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
  animations: [
    trigger('scale-up', [
      state('scale-up-start', style({
        transform: 'translateY(150px)',
        opacity: 0
      })),
      state('scale-up-end', style({
        transform: 'translateY(0px)',
        opacity: 1,
      })),
      transition('scale-up-start => scale-up-end', [
        animate('1s ease')
      ])
    ]),
  ]
})
export class CategoriesComponent implements OnInit {
  categories: Category[];
  state: string;

  @Output() categoryQuery = new EventEmitter<string>();

  constructor() {
    this.categories = [];
    this.state = 'scale-up-start';
  }

  ngOnInit() {
    fetch(`http://localhost:3000/categories/`, {
      method: 'GET'
    })
    .then((response) => {
      console.log('Response from Categories Handler Recieved');
      console.log(response);
      return response.json();
    })
    .then((data) => {
      for (let i = 0; i < data.length; ++i) {
        this.categories.push(data[i]);
      }
      setTimeout(() => {
        this.toggleScale();
      }, 500);
    })
    .catch((err) => {
      console.log(err);
    });
  }

  categoryClick($event) {
    this.categoryQuery.emit($event);
  }

  toggleScale() {
    this.state = this.state === 'scale-up-start' ? 'scale-up-end' : 'scale-up-start';
  }
}

interface Category {
  id: string;
  name: string;
  description: string;
  imgSrc: string;
}
