import {
  Component,
  OnInit,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes
} from '@angular/animations';

import {
  Subject
} from 'rxjs/Subject';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';


@Component({
  selector: 'lyonscg-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.css'],
  animations: [
    trigger('opacity', [
      state('opacity-hide', style({
        opacity: 0,
      })),
      state('opacity-show', style({
        opacity: 1,
      })),
      transition('opacity-hide => opacity-show', [
        animate('1.25s ease-in')
      ])
    ]),
  ]
})
export class ProductSearchComponent implements OnInit, OnChanges {

  @Input() query;
  @Input() invalidSearch;
  isActive: boolean;
  state: string;
  isFirstQuery = true;
  queryCurrent: string;
  queryChanged: Subject < string > = new Subject < string > ();
  @Output() receiveSearchQuery = new EventEmitter < string > ();

  constructor() {
    this.isActive = false;
    this.state = 'opacity-hide';
    this.queryChanged
      .debounceTime(500)
      .distinctUntilChanged()
      .subscribe((query) => {
        this.query = query;
        console.log('here');
        this.receiveSearchQuery.emit(query);
      });
  }

  ngOnChanges(model: any) {
    if (model.query) {
      if (!model.query.currentValue) {
        this.isActive = false;
      } else {
        this.receiveSearchQuery.emit(model.query.currentValue);
        this.isActive = true;
      }
    }
  }

  ngOnInit() {
    setTimeout(() => {
      this.toggleScale();
    }, 500);
  }

  queryUpdate(query: string) {
    this.queryChanged.next(query);
    if (!query) {
      this.isActive = false;
      this.receiveSearchQuery.emit(query);
    }
  }

  toggleScale() {
    this.state = this.state === 'opacity-hide' ? 'opacity-show' : 'opacity-hide';
  }

}
